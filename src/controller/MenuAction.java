package controller;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.MainPanel;

public class MenuAction implements ActionListener {
	
	MainPanel mainPanel;
	public MenuAction(MainPanel mainPanel) {
		this.mainPanel = mainPanel;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "draw":
			mainPanel.southPanel.setMode("Chế độ: Vẽ");
			mainPanel.centerPanel.setMode("draw");
			mainPanel.westPanel.consolePanel.addText("Chế độ: Vẽ");
			break;
		case "choose":
			mainPanel.southPanel.setMode("Chế độ: Chọn hình");
			mainPanel.centerPanel.setMode("choose");
			mainPanel.westPanel.consolePanel.addText("Chế độ: Chọn hình");
			break;
		case "delete":
			mainPanel.southPanel.setTool("Công cụ: Xoá hình");
			mainPanel.centerPanel.deleteShape();
			break;
		case "resize":
			mainPanel.southPanel.setTool("Công cụ: Resize");
			mainPanel.centerPanel.setMode("tool");
			mainPanel.centerPanel.setToolType("resize");
			mainPanel.westPanel.consolePanel.addText("Đã chọn công cụ: RESIZE");
			break;
		case "move":
			mainPanel.southPanel.setTool("Công cụ: Di chuyển");
			mainPanel.centerPanel.setMode("tool");
			mainPanel.centerPanel.setToolType("move");
			mainPanel.westPanel.consolePanel.addText("Đã chọn công cụ: DI CHUYỂN");
			break;
		case "whitetheme":
			mainPanel.centerPanel.setTheme(Color.WHITE);
			mainPanel.westPanel.consolePanel.addText("Đã đổi màu nền: SÁNG");
			break;
		case "darktheme":
			mainPanel.centerPanel.setTheme(Color.BLACK);
			mainPanel.westPanel.consolePanel.addText("Đã đổi màu nền: TỐI");
			break;
		}
		
	}
	
	

}
