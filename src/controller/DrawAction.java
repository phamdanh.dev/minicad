package controller;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.CenterPanel;

public class DrawAction implements ActionListener {
	
	CenterPanel centerPanel;
	public DrawAction(CenterPanel centerPanel) {
		this.centerPanel = centerPanel;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	public void setShapeType(String s) {
		switch (s) {
		case "Đường thẳng":
			//Line
			centerPanel.setShapeType("L");
			break;
		case "Đường tròn":
			//Circle
			centerPanel.setShapeType("C");
			break;
		case "Elip":
			//Elip
			centerPanel.setShapeType("E");
			break;
		case "Hình vuông":
			//Square
			centerPanel.setShapeType("S");
			break;
		case "Hình chữ nhật":
			//Rectangle
			centerPanel.setShapeType("R");
			break;
		}
	}
	
	public void setColor(Color color) {
		centerPanel.setColor(color);
	}

}
