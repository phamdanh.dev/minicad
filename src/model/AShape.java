package model;

import java.awt.*;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class AShape {
	private static final AtomicInteger count = new AtomicInteger(0);
	private final int shapeID;
	Color color;
	Point location;
	String name;
	ArrayList<Dot> dots;

	public AShape(int x, int y, Color color) {
		this.shapeID = count.incrementAndGet();
		location = new Point(x, y);
		this.color = color;
		dots = new ArrayList<Dot>();
	}

	public String getName() {
		return name;
	}
	
	public int getShapeID() {
		return shapeID;
	}

	public void setName(String name) {
		this.name = name;
	}


	public Color getColor() {
		return color;
	}
	
	public void setColor(Color color) {
		this.color = color;
	}

	public ArrayList<Dot> getDots() {
		return dots;
	}

	public abstract void addSecondPoint(int x, int y);

	public abstract void draw(Graphics g);

	public abstract boolean mouseSelected(Rectangle mouseRec);

	public abstract void drawDot(Graphics g);
	
	public abstract void setMove(Point point);
	
	public abstract void setResize(Point point, String position);
	
}
