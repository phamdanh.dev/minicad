package model;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;

public class Line extends AShape {
	Point secondPoint;

	public Line(int x, int y, Color color) {
		super(x, y, color);
		name = "Line";
		secondPoint = new Point(x, y);
	}

	@Override
	public void addSecondPoint(int x, int y) {
		this.secondPoint = new Point(x, y);

	}

	@Override
	public void draw(Graphics g) {
		g.setColor(color);
		g.drawLine(location.x, location.y, secondPoint.x, secondPoint.y);
	}

	@Override
	public boolean mouseSelected(Rectangle mouseRec) {
		double[] arrAB = countAB(location, secondPoint);
		for (int i = mouseRec.location.x; i <= mouseRec.secondPoint.x; i++) {
			for (int j = mouseRec.location.y; j <= mouseRec.secondPoint.y; j++) {
				if (j == (Math.round(arrAB[0] * i + arrAB[1]))) { // check diem thuoc duong thang
					return true;
				}
			}
		}
		return false;
	}

	// y = ax + b
	private double[] countAB(Point p1, Point p2) {
		double[] res = new double[2];
		res[0] = (p1.getY() - p2.getY()) / (p1.getX() - p2.getX());
		res[1] = p1.getY() - res[0] * p1.getX();
		return res;
	}

	@Override
	public void drawDot(Graphics g) {
		dots.add(new Dot(new Point(location.x, location.y), Dot.LEFT));
		dots.add(new Dot(new Point(secondPoint.x, secondPoint.y), Dot.RIGHT));
		dots.add(new Dot(new Point((location.x + secondPoint.x) / 2, (location.y + secondPoint.y) / 2), Dot.CENTER));
		for (Dot dot : dots) {
			dot.draw(g);
		}
	}

	@Override
	public void setMove(Point point) {
		int x0 = (location.x + secondPoint.x) / 2;
		int y0 = (location.y + secondPoint.y) / 2;
		location.x += point.x - x0;
		location.y += point.y - y0;
		secondPoint.x += point.x - x0;
		secondPoint.y += point.y - y0;
		dots.removeAll(dots);
	}

	@Override
	public void setResize(Point point, String position) {
		switch (position) {
		case Dot.LEFT:
			location.x = point.x;
			location.y = point.y;
			break;
		case Dot.RIGHT:
			secondPoint.x = point.x;
			secondPoint.y = point.y;
			break;

		}
		dots.removeAll(dots);
	}

}
