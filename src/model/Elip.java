package model;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

public class Elip extends AShape {
	Point secondPoint;
	double a,b, xO, yO;
	public Elip(int x, int y, Color color) {
		super(x, y, color);
		name = "Elip";
		secondPoint = new Point(x, y);
	}

	@Override
	public void addSecondPoint(int x, int y) {
		this.secondPoint = new Point(x, y);
		a = Math.abs(location.x - secondPoint.x)/2;
		b = Math.abs(location.y - secondPoint.y)/2;
		xO = location.x + (Math.abs(secondPoint.x - location.x))/2;
		yO = location.y + (Math.abs(secondPoint.y - location.y))/2;
	}

	@Override
	public void draw(Graphics g) {
		g.setColor(color);
		g.drawOval(location.x, location.y, Math.abs(location.x - secondPoint.x), Math.abs(location.y - secondPoint.y));
	}

	@Override
	public boolean mouseSelected(Rectangle mouseRec) {
		for (int i = mouseRec.location.x; i <= mouseRec.secondPoint.x; i++) {
			for (int j = mouseRec.location.y; j <= mouseRec.secondPoint.y; j++) {
				double xM = i - xO;
				double yM = yO - j;
				double x = ((double)(xM*xM)/(a*a)) + ((double)(yM*yM)/(b*b));
				if (x >= 0.95 && x <= 1.05) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public void drawDot(Graphics g) {
		dots.add(new Dot(new Point(location.x, location.y + (secondPoint.y - location.y)/2), Dot.LEFT));
		dots.add(new Dot(new Point(secondPoint.x, location.y + (secondPoint.y - location.y)/2), Dot.RIGHT));
		dots.add(new Dot(new Point(location.x + (secondPoint.x - location.x)/2, location.y + (secondPoint.y - location.y) / 2), Dot.CENTER));
		dots.add(new Dot(new Point(location.x + (secondPoint.x - location.x)/2, location.y), Dot.TOP));
		dots.add(new Dot(new Point(location.x + (secondPoint.x - location.x)/2, secondPoint.y), Dot.BOTTOM));
		for (Dot dot : dots) {
			dot.draw(g);
		}
	}

	@Override
	public void setMove(Point point) {
		int x0 = (location.x + secondPoint.x) / 2;
		int y0 = (location.y + secondPoint.y) / 2;
		location.x += point.x - x0;
		location.y += point.y - y0;
		secondPoint.x += point.x - x0;
		secondPoint.y += point.y - y0;
		dots.removeAll(dots);
	}

	@Override
	public void setResize(Point point, String position) {
		switch (position) {
		case Dot.TOP:
			location.y = point.y;
			break;
		case Dot.BOTTOM:
			secondPoint.y = point.y;
			break;
		case Dot.LEFT:
			location.x = point.x;
			break;
		case Dot.RIGHT:
			secondPoint.x = point.x;
			break;

		}
		dots.removeAll(dots);
	}


}