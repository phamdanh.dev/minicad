package model;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

public class Circle extends AShape {

	int radius = 0;

	public Circle(int x, int y, Color color) {
		super(x, y, color);
		name = "Circle";
	}

	@Override
	public void addSecondPoint(int x, int y) {
		this.radius = (int) Math.sqrt(((location.x - x) * (location.x - x)) + ((location.y - y) * (location.y - y)));

	}

	@Override
	public void draw(Graphics g) {
		g.setColor(color);
		g.drawOval(location.x - radius, location.y - radius, radius * 2, radius * 2);
	}

	@Override
	public boolean mouseSelected(Rectangle mouseRec) {
		for (int i = mouseRec.location.x; i <= mouseRec.secondPoint.x; i++) {
			for (int j = mouseRec.location.y; j <= mouseRec.secondPoint.y; j++) {
				double d = Math.round(Math.sqrt(Math.pow(i - location.x, 2) + Math.pow(j - location.y, 2)));
				if (d == radius) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public void drawDot(Graphics g) {
		dots.add(new Dot(new Point(location.x - radius, location.y), Dot.LEFT));
		dots.add(new Dot(new Point(location.x + radius, location.y), Dot.RIGHT));
		dots.add(new Dot(new Point(location.x, location.y), Dot.CENTER));
		dots.add(new Dot(new Point(location.x, location.y - radius), Dot.TOP));
		dots.add(new Dot(new Point(location.x, location.y + radius), Dot.BOTTOM));
		for (Dot dot : dots) {
			dot.draw(g);
		}
	}

	@Override
	public void setMove(Point point) {
		location.x = point.x;
		location.y = point.y;
		dots.removeAll(dots);
	}

	@Override
	public void setResize(Point point, String position) {
		addSecondPoint(point.x, point.y);
		dots.removeAll(dots);
	}


}
