package model;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

public class Dot {
	public static final String LEFT = "left";
	public static final String RIGHT = "right";
	public static final String TOP = "top";
	public static final String BOTTOM = "bottom";
	public static final String CENTER = "center";
	
	String type;
	Point point;
	public Dot(Point point, String type) {
		this.type = type;
		this.point = point;
	}
	
	public void draw(Graphics g) {
		g.setColor(Color.BLUE);
		g.fillOval(point.x - 4, point.y - 4, 8, 8);
		g.setColor(Color.WHITE);
	}

	public String getType() {
		return type;
	}

	public Point getPoint() {
		return point;
	}

	
	

}
