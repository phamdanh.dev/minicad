package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class EastPanel extends JPanel {
	InfoPanel infoPanel;
	SelectedPanel selectedPanel;
	public EastPanel(JFrame frame, MainPanel mainPanel) {
		//Generate
		infoPanel = new InfoPanel(frame, mainPanel);
		selectedPanel = new SelectedPanel(mainPanel);
		
		setPreferredSize(new Dimension(200, 0));
		setLayout(new GridLayout(2, 1));
		setBorder(BorderFactory.createLineBorder(Color.BLACK));
		add(infoPanel);
		add(selectedPanel);
	}
}
