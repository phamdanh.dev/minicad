package view;

import java.awt.Color;
import java.awt.Dimension;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

public class ConsolePanel extends JPanel {
	String s;
	JTextArea text;
	DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");  
	public ConsolePanel() {
		setBackground(Color.WHITE);
		setBorder(BorderFactory.createTitledBorder("Console"));
		
		text = new JTextArea();
		text.setEditable(false);
		text.setBackground(Color.BLACK);
		text.setForeground(Color.WHITE);
		text.setLineWrap(true);
		text.setWrapStyleWord(true);
		LocalDateTime now = LocalDateTime.now();  
		s = dtf.format(now) + ": " + "MiniCAD xin chào!\n---------------\nChế độ: Vẽ\nLoại hình: Đường thẳng";

		text.setText(s);
		JScrollPane scrollPane = new JScrollPane(text);
		scrollPane.setPreferredSize(new Dimension(190, 230));
		add(scrollPane);
	}
	
	public void addText(String str) {
		LocalDateTime now = LocalDateTime.now();  
		s += "\n---------------";
		s += "\n" + dtf.format(now) + ": " + str;
		text.setText(s);
	}
}
