package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import model.AShape;

public class SelectedPanel extends JPanel {

	JPanel listPanel;
	Set<AShape> listChoose;
	MainPanel mainPanel;
	JButton btnChooseItem;
	public SelectedPanel(MainPanel mainPanel) {
		this.mainPanel = mainPanel;
		listChoose = new HashSet<AShape>();
		setBackground(Color.WHITE);
		listPanel = new JPanel();
		listPanel.setBackground(Color.WHITE);
		listPanel.setLayout(new BoxLayout(listPanel, BoxLayout.Y_AXIS));
		setBorder(BorderFactory.createTitledBorder("Đã chọn"));
		


		JScrollPane scrollPane = new JScrollPane(listPanel, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setPreferredSize(new Dimension(190, 230));
		add(scrollPane);
	}
	
	ActionListener btnChooseAction = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			AShape shape = null;
			for (AShape aShape : listChoose) {
				if (String.valueOf(aShape.getShapeID()).equals(e.getActionCommand())) {
					shape = aShape;
					listChoose.removeAll(listChoose);
					listChoose.add(shape);
					setListChoose(listChoose);
					mainPanel.centerPanel.setListChoose((HashSet<AShape>)listChoose);
					mainPanel.centerPanel.repaint();
					break;
				}
			}
			mainPanel.eastPanel.infoPanel.setShape(shape);
		}
	};
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		listPanel.removeAll();
		for (AShape sh : listChoose) {
			btnChooseItem = new JButton(sh.getName());
			btnChooseItem.setMaximumSize(new Dimension(170, 40));
			btnChooseItem.setPreferredSize(new Dimension(170, 40));
			btnChooseItem.setActionCommand(String.valueOf(sh.getShapeID()));
			btnChooseItem.addActionListener(btnChooseAction);
			listPanel.add(btnChooseItem);
			listPanel.revalidate();
		}
	}
	
	public void setListChoose(Set<AShape> listChoose) {
		this.listChoose = listChoose;
		this.revalidate();
		this.repaint();
	}

	
}
