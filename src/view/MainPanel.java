package view;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import controller.DrawAction;

public class MainPanel extends JPanel{
	
	public SouthPanel southPanel;
	public EastPanel eastPanel;
	public WestPanel westPanel;
	public CenterPanel centerPanel;
	public DrawAction drawAction;
	public MainPanel(JFrame frame) {
		//Generate
		centerPanel = new CenterPanel(this);
		drawAction = new DrawAction(centerPanel);
		southPanel = new SouthPanel();
		westPanel = new WestPanel(frame, drawAction);
		eastPanel = new EastPanel(frame, this);
		
		setLayout(new BorderLayout());
		add(southPanel, BorderLayout.SOUTH);
		add(westPanel, BorderLayout.WEST);
		add(eastPanel, BorderLayout.EAST);
		add(centerPanel, BorderLayout.CENTER);
		
	}
	public EastPanel getEastPanel() {
		return eastPanel;
	}
	
}
