package view;

import java.awt.Dimension;

import javax.swing.JFrame;

import controller.MenuAction;

public class MiniCAD extends JFrame {
	MainPanel mainPanel;
	MyMenuBar myMenuBar;
	MenuAction menuAction;

	public MiniCAD() {
		super("My Paint");

		// Generate
		mainPanel = new MainPanel(this);
		menuAction = new MenuAction(mainPanel);
		myMenuBar = new MyMenuBar(menuAction);

		setJMenuBar(myMenuBar);
		getContentPane().add(mainPanel);
		setSize(1000, 600);
		setMinimumSize(new Dimension(800, 600));
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}

	public static void main(String[] args) {
		new MiniCAD();
	}
}
