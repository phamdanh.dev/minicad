package view;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import controller.MenuAction;

public class MyMenuBar extends JMenuBar {
	
	MenuAction menuAction;
	
	JMenu menu;
	JMenuItem menuItem;
	public MyMenuBar(MenuAction menuAction) {
		this.menuAction = menuAction;
		
		//Mode
		menu = new JMenu("Chế độ");
		menuItem = new JMenuItem("Vẽ");
		menuItem.setActionCommand("draw");
		menuItem.addActionListener(menuAction);
		menu.add(menuItem);
		menuItem = new JMenuItem("Chọn hình");
		menuItem.setActionCommand("choose");
		menuItem.addActionListener(menuAction);
		menu.add(menuItem);
		add(menu);
		
		//Tool
		menu = new JMenu("Công cụ");
		menuItem = new JMenuItem("Xoá hình");
		menuItem.setActionCommand("delete");
		menuItem.addActionListener(menuAction);
		menu.add(menuItem);
		menuItem = new JMenuItem("Resize");
		menuItem.setActionCommand("resize");
		menuItem.addActionListener(menuAction);
		menu.add(menuItem);
		menuItem = new JMenuItem("Di chuyển");
		menuItem.setActionCommand("move");
		menuItem.addActionListener(menuAction);
		menu.add(menuItem);
		add(menu);
		
		//Theme
		menu = new JMenu("Theme");
		menuItem = new JMenuItem("Nền sáng");
		menuItem.setActionCommand("whitetheme");
		menuItem.addActionListener(menuAction);
		menu.add(menuItem);
		menuItem = new JMenuItem("Nền tối");
		menuItem.setActionCommand("darktheme");
		menuItem.addActionListener(menuAction);
		menu.add(menuItem);
		add(menu);
	}

}
