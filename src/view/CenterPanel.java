package view;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import model.AShape;
import model.Circle;
import model.Dot;
import model.Elip;
import model.Line;
import model.Rectangle;
import model.Square;

public class CenterPanel extends JPanel {
	MainPanel mainPanel;
	String shapeType = "L";
	List<AShape> shapes;
	AShape lastShape;
	Color color = Color.WHITE;
	String mode = "draw";
	Rectangle mouseShape;
	HashSet<AShape> listChoose;
	boolean isPressAlt = false;
	// tool
	String toolType;
	// move
	boolean dotCenterOn = false;
	// resize
	boolean dotTopOn = false;
	boolean dotBottomOn = false;
	boolean dotLeftOn = false;
	boolean dotRightOn = false;

	public CenterPanel(MainPanel mainPanel) {
		this.mainPanel = mainPanel;
		this.setFocusable(true);
		this.requestFocusInWindow();
		this.requestFocus();
		shapes = new ArrayList<AShape>();
		listChoose = new HashSet<AShape>();

		setBackground(Color.BLACK);

		// SetKEY
		Action altPressAction = new AbstractAction() {

			@Override
			public void actionPerformed(ActionEvent e) {
				isPressAlt = true;
			}
		};
		Action altReleaseAction = new AbstractAction() {

			@Override
			public void actionPerformed(ActionEvent e) {
				isPressAlt = false;
			}
		};
		this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("alt ALT"), "altPress");
		this.getActionMap().put("altPress", altPressAction);
		this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ALT, 0, true),
				"altRelease");
		this.getActionMap().put("altRelease", altReleaseAction);

		MouseAdapter mouseAction = new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
			}

			@Override
			public void mouseMoved(MouseEvent e) {
				mainPanel.southPanel.setCoordinate(e.getX(), e.getY());
				if (mode.equals("tool")) {
					AShape shape = listChoose.iterator().next();
					for (Dot dot : shape.getDots()) {
						if ((e.getX() >= dot.getPoint().x - 4) && (e.getX() <= dot.getPoint().x + 4)
								&& (e.getY() >= dot.getPoint().y - 4) && (e.getY() <= dot.getPoint().y + 4)) {
							switch (dot.getType()) {
							case Dot.TOP: {
								setCursor(new Cursor(Cursor.N_RESIZE_CURSOR));
								dotTopOn = true;
								return;
							}
							case Dot.BOTTOM: {
								setCursor(new Cursor(Cursor.S_RESIZE_CURSOR));
								dotBottomOn = true;
								return;
							}
							case Dot.LEFT: {
								setCursor(new Cursor(Cursor.W_RESIZE_CURSOR));
								dotLeftOn = true;
								return;
							}
							case Dot.RIGHT: {
								setCursor(new Cursor(Cursor.E_RESIZE_CURSOR));
								dotRightOn = true;
								return;
							}
							case Dot.CENTER: {
								setCursor(new Cursor(Cursor.HAND_CURSOR));
								dotCenterOn = true;
								return;
							}
							}
						} else {
							setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
							dotCenterOn = false;
							dotTopOn = false;
							dotBottomOn = false;
							dotLeftOn = false;
							dotRightOn = false;
						}

					}
				}
			}

			@Override
			public void mouseExited(MouseEvent e) {
				setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
			}

			@Override
			public void mousePressed(MouseEvent e) {
				if (mode.equals("draw")) {
					switch (shapeType) {
					case "L":
						lastShape = new Line(e.getX(), e.getY(), color);
						shapes.add(lastShape);
						break;
					case "C":
						lastShape = new Circle(e.getX(), e.getY(), color);
						shapes.add(lastShape);
						break;
					case "R":
						lastShape = new Rectangle(e.getX(), e.getY(), color);
						shapes.add(lastShape);
						break;
					case "E":
						lastShape = new Elip(e.getX(), e.getY(), color);
						shapes.add(lastShape);
						break;
					case "S":
						lastShape = new Square(e.getX(), e.getY(), color);
						shapes.add(lastShape);
						break;
					}
				}
				if (mode.equals("choose")) {
					mouseShape = new Rectangle(e.getX(), e.getY(),
							getBackground() == Color.BLACK ? Color.WHITE : Color.BLACK);
				}
				repaint();
			}

			@Override
			public void mouseDragged(MouseEvent e) {
				if (mode.equals("draw")) {
					if (lastShape == null)
						return;
					lastShape.addSecondPoint(e.getX(), e.getY());
				}
				if (mode.equals("choose")) {
					if (mouseShape == null)
						return;
					mouseShape.addSecondPoint(e.getX(), e.getY());
				}
				// TOOL
				if (mode.equals("tool")) {
					if (toolType == null) {
						return;
					}
					AShape shape = listChoose.iterator().next();
					switch (toolType) {
					case "move": {
						if (dotCenterOn) {
							shape.setMove(new Point(e.getX(), e.getY()));
						}
						break;
					}
					case "resize": {
						if (dotTopOn) {
							shape.setResize(new Point(e.getX(), e.getY()), "top");
							break;
						}
						if (dotBottomOn) {
							shape.setResize(new Point(e.getX(), e.getY()), "bottom");
							break;
						}
						if (dotLeftOn) {
							shape.setResize(new Point(e.getX(), e.getY()), "left");
							break;
						}
						if (dotRightOn) {
							shape.setResize(new Point(e.getX(), e.getY()), "right");
							break;
						}
						break;
					}
					}

				}
				repaint();
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				/*
				 * Logic find shape chose here loop will find what shape item is relative to
				 * mouseShape then add them into list
				 */

				if (mode.equals("choose") && mouseShape != null) {
					if (!isPressAlt) {
						listChoose.removeAll(listChoose);
					}
					for (AShape sh : shapes) {
						if (sh.mouseSelected(mouseShape)) {
							listChoose.add(sh);
						}
					}

					// empty mouse
					mouseShape = null;

					// Send list choose to SelectedPanel
					mainPanel.getEastPanel().selectedPanel.setListChoose(listChoose);
					mainPanel.eastPanel.infoPanel.setShape(null);
				}
//				for (AShape aShape : listChoose) {
//					System.out.println(aShape.getName());
//				}
				repaint();
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				if (mode.equals("choose")) {
					if (!isPressAlt) {
						listChoose.removeAll(listChoose);
						mainPanel.getEastPanel().selectedPanel.setListChoose(listChoose);
					}
					mouseShape = new Rectangle(e.getX() - 3, e.getY() - 3, color);
					mouseShape.addSecondPoint(e.getX() + 6, e.getY() + 6);
					for (AShape sh : shapes) {
						if (sh.mouseSelected(mouseShape)) {
							listChoose.add(sh);
						}
					}

					// empty mouse
					mouseShape = null;
				}
				repaint();
			}

		};

		addMouseListener(mouseAction);
		addMouseMotionListener(mouseAction);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		for (AShape sh : shapes) {
			sh.draw(g);
		}
		if (mouseShape != null) {
			mouseShape.draw(g);
		}
		if (!listChoose.isEmpty()) {
			for (AShape sh : listChoose) {
				sh.drawDot(g);
			}
		}
	}

	public void setTheme(Color color) {
		this.setBackground(color);
	}

	public void setShapeType(String type) {
		this.shapeType = type;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public HashSet<AShape> getListChoose() {
		return listChoose;
	}

	public void setListChoose(HashSet<AShape> listChoose) {
		this.listChoose = listChoose;
	}

	public void setToolType(String s) {
		this.toolType = s;
	}

	public void setMode(String s) {
		if (s.equals("draw")) {
			listChoose.removeAll(listChoose);
			mainPanel.getEastPanel().selectedPanel.setListChoose(listChoose);
		}
		if (s.equals("tool")) {
			if (listChoose.size() > 1 || listChoose.size() == 0) {
				// Console: Ban chi duoc chon mot hinh khi dung cong cu nay
				return;
			}
		}
		this.mode = s;
		//reset tool
		toolType = null;
		repaint();
	}

	public void deleteShape() {
		if (shapes.size() > 0) {
			for (AShape sh : shapes) {
				for (AShape shChoose : listChoose) {
					if (sh.getShapeID() == shChoose.getShapeID()) {
						mainPanel.westPanel.consolePanel.addText("Đã xoá hình " + sh.getName());
						shapes.remove(sh);
						listChoose.remove(shChoose);
						mainPanel.getEastPanel().selectedPanel.setListChoose(listChoose);
						mainPanel.eastPanel.infoPanel.setShape(null);
						if (listChoose.size() == 0) {
							setMode("choose");
							repaint();
							return;
						}
						break;
					}
				}
			}
		}
		setMode("choose");
		repaint();
	}
}
