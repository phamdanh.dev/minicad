package view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.DrawAction;

public class WestPanel extends JPanel {

	DrawToolPanel drawToolPanel;
	public ConsolePanel consolePanel;
	DrawAction drawAction;
	public WestPanel(JFrame frame, DrawAction drawAction) {
		//Generate
		this.drawAction = drawAction;
		drawToolPanel = new DrawToolPanel(frame, drawAction);
		consolePanel = new ConsolePanel();
		
		setPreferredSize(new Dimension(200, 0));
		setLayout(new GridLayout(2, 1));
		setBorder(BorderFactory.createLineBorder(Color.BLACK));
		add(drawToolPanel);
		add(consolePanel);
	}

}
