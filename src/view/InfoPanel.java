package view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import model.AShape;

public class InfoPanel extends JPanel {
	MainPanel mainPanel;
	JTextField txtName;
	JButton btnColor;
	JButton btnChooseColor;
	JButton btnUpdate;
	Color color;
	AShape shape;
	public InfoPanel(JFrame frame, MainPanel mainPanel) {
		this.mainPanel = mainPanel;
		// Generate
		setBackground(Color.WHITE);
		color = Color.WHITE;
		btnColor = new JButton();
		btnColor.setBackground(Color.WHITE);
		btnColor.setPreferredSize(new Dimension(30, 30));
		btnColor.setMaximumSize(new Dimension(30, 30));
		btnChooseColor = new JButton("Chọn màu");
		btnChooseColor.setBackground(Color.WHITE);
		txtName = new JTextField();
		txtName.setPreferredSize(new Dimension(500, 30));
		txtName.setMaximumSize(new Dimension(500, 30));
		btnUpdate = new JButton("Cập nhật");
		btnUpdate.addActionListener(btnUpdateAction);
		btnUpdate.setPreferredSize(new Dimension(180, 30));
		btnUpdate.setMaximumSize(new Dimension(180, 30));
		btnUpdate.setBackground(Color.WHITE);
		JLabel lbPadding = new JLabel();
		JLabel lbPadding2 = new JLabel();
		JLabel lbPadding3 = new JLabel();
		lbPadding.setBorder(new EmptyBorder(10, 0, 0, 0));
		lbPadding2.setBorder(new EmptyBorder(10, 0, 0, 0));
		lbPadding3.setBorder(new EmptyBorder(50, 0, 0, 0));
		
		//main
		setBorder(BorderFactory.createTitledBorder("Chi tiết hình"));
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		add(new JLabel("Tên hình"));
		add(txtName);
		add(lbPadding);
		add(new JLabel("Màu"));
		
		btnColor.setBackground(Color.WHITE);
		btnChooseColor.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Color newColor = JColorChooser.showDialog(frame, "Chọn màu", Color.WHITE);
				if (newColor != null) {
					color = newColor;
					btnColor.setBackground(color);
				}
			}
		});
		add(btnColor);
		add(lbPadding2);
		add(btnChooseColor);
		add(lbPadding3);
		add(btnUpdate);
		
	}
	
	ActionListener btnUpdateAction = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			if (shape != null) {
				shape.setName(txtName.getText());
				shape.setColor(color);
				mainPanel.centerPanel.repaint();
				mainPanel.westPanel.consolePanel.addText("Đã cập nhật hình " + shape.getName());
			}

		}
	};
	
	public void setShape(AShape shape) {
		
		if (shape != null) {
			this.shape = shape;
			txtName.setText(shape.getName());
			color = shape.getColor();
			btnColor.setBackground(color);
		} else {
			txtName.setText("");
			color = Color.WHITE;
			btnColor.setBackground(color);
		}
		
	}
}
