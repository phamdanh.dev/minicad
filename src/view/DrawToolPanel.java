package view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import controller.DrawAction;

public class DrawToolPanel extends JPanel {
	DrawAction drawAction;
	
	JButton btnColor;
	JButton btnChooseColor;
	Color color;
	JComboBox<String> cbbHinh;

	public DrawToolPanel(JFrame frame, DrawAction drawAction) {
		this.drawAction = drawAction;
		// Generate
		setBackground(Color.WHITE);
		color = Color.WHITE;
		btnColor = new JButton();
		btnColor.setBackground(Color.WHITE);
		btnColor.setPreferredSize(new Dimension(30, 30));
		btnColor.setMaximumSize(new Dimension(30, 30));
		btnChooseColor = new JButton("Chọn màu");
		btnChooseColor.setBackground(Color.WHITE);
		cbbHinh = new JComboBox<String>();
		
		ActionListener actionCombobox = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (cbbHinh.getSelectedIndex() != -1) {
					drawAction.setShapeType(cbbHinh.getItemAt(cbbHinh.getSelectedIndex()));
				}
				
			}
		};
		
		cbbHinh.addItem("Đường thẳng");
		cbbHinh.addItem("Đường tròn");
		cbbHinh.addItem("Elip");
		cbbHinh.addItem("Hình vuông");
		cbbHinh.addItem("Hình chữ nhật");
		cbbHinh.addActionListener(actionCombobox);
		cbbHinh.setMinimumSize(new Dimension(0, 30));
		cbbHinh.setMaximumSize(new Dimension(400, 30));
		cbbHinh.setBackground(Color.WHITE);
		JLabel lbPadding = new JLabel();
		JLabel lbPadding2 = new JLabel();
		lbPadding.setBorder(new EmptyBorder(30, 0, 0, 0));
		lbPadding2.setBorder(new EmptyBorder(10, 0, 0, 0));

		// main
		setBorder(BorderFactory.createTitledBorder("Công cụ vẽ"));
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		add(new JLabel("Chọn hình"));
		add(cbbHinh);
		add(lbPadding);
		add(new JLabel("Màu"));

		btnColor.setBackground(Color.WHITE);
		btnChooseColor.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Color newColor = JColorChooser.showDialog(frame, "Chọn màu", Color.WHITE);
				if (newColor != null) {
					color = newColor;
					btnColor.setBackground(color);
					drawAction.setColor(newColor);
				}
			}
		});
		add(btnColor);
		add(lbPadding2);
		add(btnChooseColor);
	}
}
