package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class SouthPanel extends JPanel {
	
	JLabel coordinates;
	JLabel mode;
	JLabel tool;
	public SouthPanel() {
		//Generate
		coordinates = new JLabel("          Coordinates: x: 0, y: 0");
		mode = new JLabel("Chế độ: Vẽ");
		tool = new JLabel("Công cụ: Không");
		
		setPreferredSize(new Dimension(600, 30));
		setLayout(new GridLayout(1, 5));
		setBorder(BorderFactory.createLineBorder(Color.BLACK));
		add(coordinates);
		add(new JLabel());
		add(mode);
		add(tool);
		add(new JLabel());
		
	}
	
	public void setCoordinates(String s) {
		this.coordinates.setText(s);
	}
	
	public void setMode(String s) {
		this.mode.setText(s);
	}
	
	public void setTool(String s) {
		this.tool.setText(s);
	}
	
	public void setCoordinate(int x, int y) {
		coordinates.setText("          Coordinates: x: " + x + ", y: "+ y);
	}

}
